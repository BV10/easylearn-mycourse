package main.java;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import main.java.View.Authorization.StartPageControl;

import java.io.IOException;

public class MainApp extends Application {

    private Stage primaryStage;

    private AnchorPane startPage;

    public static void main(String[] args) {
        launch();
    }

    public static void addIconAlert(Alert alert) {
        // Get the Stage.
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        // Add a custom icon.
        stage.getIcons().add(new Image("/resources/Images/IconApp.png"));
    }

    public static void errorLoadFXML() {
        // create dialog window error
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("EasyLearn");
        alert.setHeaderText("Error during load FXML file");

        addIconAlert(alert);

        alert.showAndWait();
    }

    public void start(Stage primaryStage) {
        if (launchServer()) { // server launches successfully
            this.primaryStage = primaryStage;
            primaryStage.setTitle("EasyLearn");
            primaryStage.setResizable(false); // prohibit resize
            primaryStage.getIcons().add(new Image("/resources/Images/IconApp.png")); // set icon for app

            initStartPage();
        } else {
            primaryStage.close();
        }
    }

    private void initStartPage() {
        try {
            //Load start page from fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("View/Authorization/StartPageView.fxml"));
            startPage = (AnchorPane) loader.load();
            //

            // show Scene - start page
            primaryStage.setScene(new Scene(startPage));
            primaryStage.show();
            //

            // access controller to MainApp
            StartPageControl controller = loader.getController();
            controller.setCurrStage(primaryStage);

        } catch (IOException io) {
            errorLoadFXML();

        }
    }

    private boolean launchServer() {
        try {
            //launch server
            new ProcessBuilder("C:\\Program Files\\MongoDB\\Server\\3.4\\bin\\mongod.exe").start();
            return true;

        } catch (IOException io) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(primaryStage);
            alert.setTitle("Ошибка при загрузке");
            alert.setHeaderText("Ошибка при запуске сервера");
            alert.showAndWait();

            addIconAlert(alert);

            return false;
        }
    }

}
