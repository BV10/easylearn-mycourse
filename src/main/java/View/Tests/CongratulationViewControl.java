package main.java.View.Tests;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import main.java.Core.dao.UserDao;
import main.java.Core.model.Theme;
import main.java.MainApp;
import main.java.View.ChoiceThemeAndWords.ChoiceThemesControl;
import main.java.View.ChoiceThemeAndWords.ChoiceWordControl;

import java.io.IOException;
import java.util.List;

public class CongratulationViewControl {
    private Stage currStage;

    private Theme theme;

    private MediaPlayer mediaPlayer; // for stop music

    private List<Theme> themeList;

    @FXML
    private Button buttonCont;


    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public void setCurrStage(Stage currStage) {
        this.currStage = currStage;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public void setThemeList(List<Theme> themeList) {
        this.themeList = themeList;
    }

    public void updatePoints() {
        // update user points in app
        Long currentPoints = ChoiceThemesControl.getUser().getPoints();
        ChoiceThemesControl.getUser().setPoints(currentPoints + 20);

        //update user point in database
        UserDao userDao = new UserDao();
        userDao.updateUserPoints(ChoiceThemesControl.getUser());

    }

    public void updateLearnMatherial() {
        // last theme was learnt and all words were learnt
        if (ChoiceThemesControl.getUser().getLearntThemes() == 4 & ChoiceThemesControl.getUser().getLearnBlockWords().get("MainVerbs") != null && ChoiceThemesControl.getUser().getLearnBlockWords().get("MainVerbs") == 10) {
            return;
        }


        // get name of next theme
        int indexCurTheme = themeList.indexOf(theme);
        String nextNameTheme = "";
        if (indexCurTheme + 1 != themeList.size()) // last index theme not out the bound list
            nextNameTheme = themeList.get(indexCurTheme + 1).getName();

        // if learnt all 10 blocks and next theme is not accessible, do able next theme and return
        if (ChoiceThemesControl.getUser().getLearnBlockWords().get(theme.getName()) == 10 & !ChoiceThemesControl.getUser().getLearnBlockWords().containsKey(nextNameTheme) & !nextNameTheme.equals("")) {

            int currLearnThemes = ChoiceThemesControl.getUser().getLearntThemes();
            ChoiceThemesControl.getUser().setLearntThemes(currLearnThemes + 1); // do able new theme

            ChoiceThemesControl.getUser().getLearnBlockWords().put(nextNameTheme, 1); // do able words in new theme

            new UserDao().updateUser(ChoiceThemesControl.getUser()); // update user in db

            return;
        } else if (ChoiceThemesControl.getUser().getLearnBlockWords().get(theme.getName()) == 10) { // next theme is able
            return; // do nothing
        }


        // do able next block word
        int currLearnWords = ChoiceThemesControl.getUser().getLearnBlockWords().get(theme.getName());
        ChoiceThemesControl.getUser().getLearnBlockWords().put(theme.getName(), currLearnWords + 1);

        new UserDao().updateUser(ChoiceThemesControl.getUser());
    }


    @FXML
    private void handleClickButCont() {
        mediaPlayer.stop();
        // open view words FXML
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("View/ChoiceThemeAndWords/ChoiceWordView.fxml"));
            AnchorPane pane = (AnchorPane) loader.load();

            ChoiceWordControl control = loader.getController();
            control.setCurrStage(currStage);
            control.setCurrTheme(theme);
            control.showWords();

            currStage.setScene(new Scene(pane));
        } catch (IOException io) {
            MainApp.errorLoadFXML();
        }

    }

    @FXML
    private void mouseMovedButCont() {
        buttonCont.setStyle("-fx-effect: inherit");
    }

    @FXML
    private void mouseExitButCont() {
        buttonCont.setStyle("");
    }

}
