package main.java.View.Tests;


import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import main.java.Core.model.Theme;
import main.java.Core.model.Word;
import main.java.MainApp;
import main.java.View.ChoiceThemeAndWords.ChoiceWordControl;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class IntroductionWithWordControll {
    private Stage currStage;
    private Theme theme;
    private List<Word> wordList = new ArrayList<>();
    private int currWord = 0;


    @FXML
    private AnchorPane pane;

    @FXML
    private Label labelWord;

    @FXML
    private Label labelSent;

    @FXML
    private Label labelTraanslSent;

    @FXML
    private Button butPrevPage;

    @FXML
    private Button butNextAssign;

    @FXML
    private void initialize(){
        pane.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if(event.getCode() == KeyCode.BACK_SPACE){
                handlePrevPage();
            }
            if(event.getCode() == KeyCode.ENTER){
                handleNextAssign();
            }
        });
    }

    private ImageView imageView = new ImageView();


    public void setWordList(List<Word> wordList) {
        this.wordList = wordList;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public void setCurrStage(Stage currStage) {
        this.currStage = currStage;
    }

    public void showIntroduct(){
        showImage();
        showWord();
        showSents();
    }


    private void showImage() {
        File currentDir = new File("."); // get path to project
        File file = new File(currentDir.getAbsoluteFile() +"\\src\\resources\\Themes\\"+ theme.getName() + "\\" + "Images\\" + wordList.get(currWord).getWord() + ".jpg");
        Image image = new Image(file.toURI().toString());

        imageView = new ImageView(image);
        imageView.setFitHeight(300);
        imageView.setFitWidth(400);
        imageView.setY(30);
        imageView.setX((pane.getPrefWidth() - imageView.getFitWidth())/2);
        pane.getChildren().add(imageView);
    }

    private void showWord(){
        labelWord.setText(wordList.get(currWord).getWord() + " (" + wordList.get(currWord).getTranslWord() + ")");

    }

    private void showSents(){

        // get sent and transl sent on 2
        String[] sent = wordList.get(currWord).getSentence().split("\n", 2);
        //

         // add sents to labels
        labelSent.setText(sent[0]);
        labelTraanslSent.setText(sent[1]);
    }

    @FXML
    private void handlePrevPage(){
        if(currWord == 0){
            // open choice words
            try{
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainApp.class.getResource("View/ChoiceThemeAndWords/ChoiceWordView.fxml"));
                AnchorPane pane = loader.load();


                ChoiceWordControl control = loader.getController();
                control.setCurrStage(currStage);
                control.setCurrTheme(theme);
                control.showWords();

                currStage.setScene(new Scene(pane));
            } catch (IOException io){
                MainApp.errorLoadFXML();
            }
        }
        else {
            currWord--;
            pane.getChildren().remove(imageView);
            showImage();
            showWord();
            showSents();
        }
    }

    @FXML
    private void handleNextAssign(){
        currWord++;
        if(currWord<4){
            pane.getChildren().remove(imageView);
            showImage();
            showWord();
            showSents();
        }
        else{
            RandTest.showRandTest(currStage, wordList, theme.getName());
        }
    }

    @FXML
    private void mouseMovedPrevPage(){
        butPrevPage.setStyle("-fx-effect: inherit");
    }

    @FXML
    private void mouseExitPrevPage(){
        butPrevPage.setStyle("");
    }

    @FXML
    private void mouseMovedNextAssign(){
        butNextAssign.setStyle("-fx-effect: inherit");
    }

    @FXML
    private void mouseExitNextAssign(){
        butNextAssign.setStyle("");
    }
}
