package main.java.View.Tests;


import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import main.java.Core.dao.ThemeDao;
import main.java.Core.model.Theme;
import main.java.Core.model.Word;
import main.java.MainApp;
import main.java.View.ChoiceThemeAndWords.ChoiceThemesControl;
import main.java.View.ChoiceThemeAndWords.ChoiceWordControl;

import java.io.File;
import java.io.IOException;
import java.util.*;

enum LangWordTest {ENGLISH, RUSSIAN}; // for define type test

public class TranslatingWordControl {
    @FXML
    private AnchorPane pane;

    @FXML
    private Button buttonExitTest;

    private Stage currStage;

    // for open words view
    String nameTheme = new String();

    // current number word
    private int currNumbWord = 0;
    // for type of test
    private LangWordTest langWordTest = LangWordTest.RUSSIAN;

    private List<Word> listWord = new ArrayList<>();

    List<Theme> themeList = new ThemeDao().listTheme();

    @FXML
    Label labelWord;

    // get random filled array
    public static int[] genRandArr(int len) {
        int[] arr = new int[len];
        Random random = new Random();

        arr[0] = random.nextInt(len);
        int temp = random.nextInt(len);
        int i, j;

        // fill array
        for (i = 1; i < len; i++) {
            for (j = 0; j < i; j++) {
                if (temp == arr[j]) { // the same elem
                    temp = random.nextInt(len);
                    j = -1;
                    continue;
                }
            }
            arr[i] = temp;
        }

        return arr;
    }

    //dialog window for confirm of exit form account
    public static boolean confirmExitFromTest() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        MainApp.addIconAlert(alert);
        alert.setTitle("Подтверждение");
        alert.setHeaderText("Вы хотите завершить тестирование?");
        alert.setContentText("Баллы за тест не будут засчитаны.");
        Optional<ButtonType> result = alert.showAndWait(); // get pressed button
        if (result.get() == ButtonType.OK) {
            return true;
        } else {
            return false;
        }
    }

    // open page with words
    public static void openWordsView(Stage stage, String nameTheme) {
        // open view words FXML
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ChoiceThemesControl.class.getResource("ChoiceWordView.fxml"));
            AnchorPane pane = (AnchorPane) loader.load();

            ChoiceWordControl control = loader.getController();
            control.setCurrStage(stage);
            control.setCurrTheme(new ThemeDao().getTheme(nameTheme));
            control.showWords();

            stage.setScene(new Scene(pane));
        } catch (IOException io) {
            MainApp.errorLoadFXML();
        }
    }

    public void setNameCurrTheme(String nameTheme) {
        this.nameTheme = nameTheme;
    }

    public void setCurrStage(Stage currStage) {
        this.currStage = currStage;
    }

    public void setCurrNumbWord(int currNumbWord) {
        this.currNumbWord = currNumbWord;
    }

    public void setLangWordTest(LangWordTest langWordTest) {
        this.langWordTest = langWordTest;
    }

    public void setListWord(List<Word> listWord) {
        this.listWord = listWord;
    }

    public void showTest() {
        labelWord.setText(getWordCurLang(listWord.get(currNumbWord)));
        createButtTest(langWordTest);
    }


    @FXML
    private void hadleClickExitTestBut() {
        if (!confirmExitFromTest()) // query on confirm exit from test
            return;
        openWordsView(currStage, nameTheme); // open Words View
    }

    @FXML
    private void mouseMovedExTestBut() {
        buttonExitTest.setStyle("-fx-effect: inherit");
    }

    @FXML
    private void mouseExitExTestBut() {
        buttonExitTest.setStyle("");
    }

    private void createButtTest(LangWordTest langWordTest) {

        // create 4 buttons
        List<Button> buttonList = new ArrayList<>();
        // add current translated word in list
        Button button = new Button(getTranslWord(listWord.get(currNumbWord)));
        // add event for button
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                handleActButton(button);
            }
        });

        // highlight
        button.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setStyle("-fx-effect: inherit");
            }
        });

        button.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setStyle("");
            }
        });

        button.setFont(Font.font("System", 25));
        button.setPrefHeight(85);
        button.setPrefWidth(190);
        buttonList.add(button);
        //

        for (int i = 0; i < 3; i++) {

            Button othButton = new Button();
            othButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    handleActButton(othButton);

                }
            });

            // highlight button
            othButton.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    othButton.setStyle("-fx-effect: inherit");
                }
            });

            othButton.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    othButton.setStyle("");
                }
            });
            // get rand word
            /** this str need changed!! after add data in database*/
            // init button unique word
            othButton.setText(initUniqueWord(buttonList, othButton));
            //
            othButton.setFont(Font.font("System", 25));
            othButton.setPrefHeight(85);
            othButton.setPrefWidth(190);
            buttonList.add(othButton);
        }

        // rand location of words
        int[] arr = genRandArr(4);
        // show buttons on stage

        HBox hBox1 = new HBox();
        hBox1.getChildren().add(buttonList.get(arr[0]));
        hBox1.getChildren().add(buttonList.get(arr[1]));
        hBox1.setSpacing(200);
        hBox1.setLayoutX(110);
        hBox1.setLayoutY(300);
        pane.getChildren().add(hBox1);

        HBox hBox2 = new HBox();
        hBox2.getChildren().add(buttonList.get(arr[2]));
        hBox2.getChildren().add(buttonList.get(arr[3]));
        hBox2.setSpacing(200);
        hBox2.setLayoutX(110);
        hBox2.setLayoutY(450);
        pane.getChildren().add(hBox2);
    }

    // logic handle buttons tests
    private void handleActButton(Button button) {
        // check correct translating
        if (button.getText().equals(getTranslWord(listWord.get(currNumbWord)))) { // correctly
            //for correct continue work
            button.setDisable(true);
            // congratulation
            File curDir = new File(".");
            String musicFile = curDir.getAbsolutePath() + "\\src\\resources\\correct.wav";
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();

            // do delay
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            mediaPlayer.stop();
                            showNextTest();
                        }
                    });
                }
            }, 1000);
            //showNextTest();
        } else { //incorrectly
            button.setStyle("-fx-effect: dropshadow(gaussian, red,10, 0.7, 0, 0)");
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    button.setStyle("");
                }
            }, 800);
        }

    }

    private void showNextTest() {
        RandTest.showRandTest(currStage, listWord, nameTheme);
    }

    // get translate of word depend LanguageTransl
    private String getTranslWord(Word word) {
        if (langWordTest == LangWordTest.ENGLISH) {
            return word.getTranslWord();
        } else {
            return word.getWord();
        }
    }

    // init button unique word
    private String initUniqueWord(List<Button> buttonList, Button button) {
        Random random = new Random();
        ListIterator<Button> iterator = buttonList.listIterator();

        String uniqueWord = new String(getTranslWord(themeList.get(random.nextInt(2) + 1).getWordList().get(random.nextInt(40))));

        while (iterator.hasNext()) {
            if (iterator.next().getText().equals(uniqueWord)) {
                uniqueWord = getTranslWord(themeList.get(random.nextInt(2) + 1).getWordList().get(random.nextInt(40)));
                // go to back
                while (iterator.hasPrevious()) {
                    iterator.previous();
                }
            }
        }
        return uniqueWord;
    }

    // get word for current language
    private String getWordCurLang(Word word) {
        if (langWordTest == LangWordTest.ENGLISH) {
            return word.getWord();
        } else {
            return word.getTranslWord();
        }
    }

}
