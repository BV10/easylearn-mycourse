package main.java.View.Tests;


import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import main.java.Core.model.Word;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class TranslatingWordInputControl {

    private Stage currStage;

    // current number word
    private int currNumbWord = 0;
    // for type of test
    private LangWordTest langWordTest = LangWordTest.RUSSIAN;

    private List<Word> listWord = new ArrayList<>();

    private String nameTheme = new String();

    // for image
    private ImageView imageView;

    @FXML
    private AnchorPane pane;

    @FXML
    private Label labelWord;

    @FXML
    private TextField textFieldWord;

    @FXML
    private Button buttonCheck;

    @FXML
    private Button buttonExitTest;


    public void setCurrStage(Stage currStage) {
        this.currStage = currStage;
    }

    public void setCurrNumbWord(int currNumbWord) {
        this.currNumbWord = currNumbWord;
    }

    public void setLangWordTest(LangWordTest langWordTest) {
        this.langWordTest = langWordTest;
    }

    public void setListWord(List<Word> listWord) {
        this.listWord = listWord;
    }

    public void setThemeName(String themeName) {
        this.nameTheme = themeName;
    }

    public void showTest() {
        showImage();
        showWord();
    }


    @FXML
    private void initialize(){
        textFieldWord.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if(event.getCode() == KeyCode.ENTER){
                handleClickCheckButton();
            }
        });
    }

    @FXML
    private void hadleClickExitTestBut() {
        if (!TranslatingWordControl.confirmExitFromTest()) // query on confirm exit from test
            return;
       TranslatingWordControl.openWordsView(currStage, nameTheme);
    }

    @FXML
    private void mouseMovedExTestBut() {
        buttonExitTest.setStyle("-fx-effect: inherit");
    }

    @FXML
    private void mouseExitExTestBut() {
        buttonExitTest.setStyle("");
    }

    @FXML
    private void handleClickCheckButton(){
        if(textFieldWord.getText().equalsIgnoreCase(getTranslWord(listWord.get(currNumbWord)))){ // correctly
            // for correct work program
            buttonCheck.setDisable(true);
            textFieldWord.setDisable(true);
            // congratulation
            File curDir = new File(".");
            String musicFile = curDir.getAbsolutePath() + "\\src\\resources\\correct.wav";
            Media sound = new Media(new File(musicFile).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            mediaPlayer.stop();
                            RandTest.showRandTest(currStage, listWord, nameTheme);
                        }
                    });
                }
            }, 1000);
        } else{
            textFieldWord.setStyle("-fx-effect: dropshadow(gaussian, red, 9, 0.9, 0, 0)");

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            textFieldWord.setStyle("");
                            showHelpWord();
                        }
                    });
                }
            }, 400);
        }
    }

    @FXML
    private void mouseMoveButCheck(){
        buttonCheck.setStyle("-fx-effect: inherit");
    }

    @FXML
    private void mouseExitButCheck(){
        buttonCheck.setStyle("");
    }

    private void showImage() {
        File currentDir = new File("."); // get path to project
        File file = new File(currentDir.getAbsoluteFile() +"\\src\\resources\\Themes\\"+ nameTheme + "\\" + "Images\\" + listWord.get(currNumbWord).getWord() + ".jpg");
        Image image = new Image(file.toURI().toString());

        imageView = new ImageView(image);
        imageView.setFitHeight(220);
        imageView.setFitWidth(280);
        imageView.setY(110);
        imageView.setX((pane.getPrefWidth() - imageView.getFitWidth())/2);
        pane.getChildren().add(imageView);
    }

    private void showWord(){
        labelWord.setText(getWordCurLang(listWord.get(currNumbWord)));
    }

    // get word for current language
    private String getWordCurLang(Word word) {
        if (langWordTest == LangWordTest.ENGLISH) {
            return word.getWord();
        } else {
            return word.getTranslWord();
        }
    }

    // get translate of word depend LanguageTransl
    private String getTranslWord(Word word) {
        if (langWordTest == LangWordTest.ENGLISH) {
            return word.getTranslWord();
        } else {
            return word.getWord();
        }
    }

    // show help if user do mistake
    private void showHelpWord(){
        // location of image
        File currentDir = new File("."); // get path to project
        File file = new File(currentDir.getAbsoluteFile() + "\\src\\resources\\Images\\help.png");
        Image image = new Image(file.toURI().toString());

        ImageView imageView1 = new ImageView(image);
        // position of help
        imageView1.setLayoutX(textFieldWord.getLayoutX() + textFieldWord.getPrefWidth() + 20);
        imageView1.setLayoutY(textFieldWord.getLayoutY() - 2);
        // size image view
        imageView1.setFitWidth(40);
        imageView1.setFitHeight(40);




        // handle click on imageView - help
        imageView1.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                // show correct transl

                labelWord.setText(getTranslWord(listWord.get(currNumbWord)));
                labelWord.setStyle(" -fx-font-size: 33;\n" +
                        "-fx-font-weight: bold;\n" +
                        "-fx-text-fill: blue;\n" +
                        "-fx-font-family: \"Arial\";");


                // do delay
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                labelWord.setText(getWordCurLang(listWord.get(currNumbWord)));
                                labelWord.setStyle(" -fx-font-size: 30;\n" +
                                        "    -fx-font-weight: bold;\n" +
                                        "-fx-text-fill: black;\n" +
                                        "-fx-font-family: \"Arial\";");
                            }
                        });
                    }
                }, 1000);
            }
        });

        pane.getChildren().add(imageView1);
    }

}
