package main.java.View.Tests;


import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import main.java.Core.dao.ThemeDao;
import main.java.Core.model.Theme;
import main.java.Core.model.Word;
import main.java.MainApp;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;

public class RandTest {
    private static boolean wasSortArrays = false; // check sort array for every test
    private static int[] randNumbWords = new int[4];
    private static int[][] randNumbTestForWord = new int[4][5]; // for 4 words 5 tests
    private static int currNumbArrWord = 0; // current position in array of words
    private static int currNumbArrTest = 0; // current position in test arr


    public static void showRandTest(Stage stage, List<Word> wordList, String themeName) {
        if (!wasSortArrays) { // sort rand tests
            randNumbWords = TranslatingWordControl.genRandArr(4);//sort rand numb words
            wasSortArrays = true; // occurs sort
            // sort rand numb tests
            for (int i = 0; i < 4; i++) {
                randNumbTestForWord[i] = TranslatingWordControl.genRandArr(5);
            }
        }

        if (currNumbArrWord > 3) { // all words passed block of test
            currNumbArrWord = 0;
            currNumbArrTest++; // go to next tests
        }

        if (currNumbArrTest > 4) { // passed all tests
            //set beg values
            wasSortArrays = false;
            currNumbArrWord = 0;
            currNumbArrTest = 0;
            showCongratulation(stage, new ThemeDao().getTheme(themeName));
            return;
        }

        showTest(stage, wordList, themeName);
        currNumbArrWord++;
    }

    private static void showTest(Stage stage, List<Word> wordList, String themeName) {
        String nameTestFXML;
        switch (randNumbTestForWord[randNumbWords[currNumbArrWord]][currNumbArrTest]) {
            case 0:
                nameTestFXML = "AppropTransl.fxml";
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(MainApp.class.getResource("View/Tests/" + nameTestFXML));
                    AnchorPane pane = fxmlLoader.load();

                    stage.setScene(new Scene(pane));

                    AppropTranslControl control = fxmlLoader.getController();
                    control.setNameTheme(themeName);
                    control.setCurrStage(stage);
                    control.setWordList(wordList);
                    control.showTest();
                } catch (IOException io) {
                    MainApp.errorLoadFXML();
                }
                break;
            case 1:
                nameTestFXML = "TranslatingWord.fxml";
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(MainApp.class.getResource("View/Tests/" + nameTestFXML));
                    AnchorPane pane = fxmlLoader.load();

                    stage.setScene(new Scene(pane));

                    TranslatingWordControl control = fxmlLoader.getController();
                    control.setNameCurrTheme(themeName);
                    control.setCurrStage(stage);
                    control.setListWord(wordList);
                    control.setCurrNumbWord(randNumbWords[currNumbArrWord]);
                    control.setLangWordTest(LangWordTest.ENGLISH);
                    control.showTest();
                } catch (IOException io) {
                    MainApp.errorLoadFXML();
                }
                break;
            case 2:
                nameTestFXML = "TranslatingWord.fxml";
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(MainApp.class.getResource("View/Tests/" + nameTestFXML));
                    AnchorPane pane = fxmlLoader.load();

                    stage.setScene(new Scene(pane));

                    TranslatingWordControl control = fxmlLoader.getController();
                    control.setNameCurrTheme(themeName);
                    control.setCurrStage(stage);
                    control.setListWord(wordList);
                    control.setCurrNumbWord(randNumbWords[currNumbArrWord]);
                    control.setLangWordTest(LangWordTest.RUSSIAN);
                    control.showTest();
                } catch (IOException io) {
                    MainApp.errorLoadFXML();
                }
                break;
            case 3:
                nameTestFXML = "TranslatingWordInput.fxml";
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(MainApp.class.getResource("View/Tests/" + nameTestFXML));
                    AnchorPane pane = fxmlLoader.load();

                    stage.setScene(new Scene(pane));

                    TranslatingWordInputControl control = fxmlLoader.getController();
                    control.setThemeName(themeName);
                    control.setCurrStage(stage);
                    control.setListWord(wordList);
                    control.setCurrNumbWord(randNumbWords[currNumbArrWord]);
                    control.setLangWordTest(LangWordTest.RUSSIAN);
                    control.showTest();
                } catch (IOException io) {
                    MainApp.errorLoadFXML();
                }
                break;
            case 4:
                nameTestFXML = "TranslatingWordInput.fxml";
                try {
                    FXMLLoader fxmlLoader = new FXMLLoader();
                    fxmlLoader.setLocation(MainApp.class.getResource("View/Tests/" + nameTestFXML));
                    AnchorPane pane = fxmlLoader.load();

                    stage.setScene(new Scene(pane));

                    TranslatingWordInputControl control = fxmlLoader.getController();
                    control.setThemeName(themeName);
                    control.setCurrStage(stage);
                    control.setListWord(wordList);
                    control.setCurrNumbWord(randNumbWords[currNumbArrWord]);
                    control.setLangWordTest(LangWordTest.ENGLISH);
                    control.showTest();
                } catch (IOException io) {
                    MainApp.errorLoadFXML();
                }
                break;
        }
    }

    private static void showCongratulation(Stage stage, Theme theme) {


        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(MainApp.class.getResource("View/Tests/CongratulationView.fxml"));
            AnchorPane pane = fxmlLoader.load();

            CongratulationViewControl control = fxmlLoader.getController();
            control.setTheme(theme);
            control.setThemeList(new ThemeDao().listTheme());
            control.setCurrStage(stage);
            control.setMediaPlayer(playMusicWin());
            control.updatePoints();
            control.updateLearnMatherial();

            stage.setScene(new Scene(pane));

        } catch (IOException io) {
            MainApp.errorLoadFXML();
        }
    }

    private static MediaPlayer playMusicWin(){
        File curDir = new File(".");
        String musicsFile = curDir.getAbsolutePath() + "\\src\\resources\\MusicCongr\\";

        String[] nameSongs = {"Eroina", "Feder_-_Lordly", "Felt_so_Right", "Get_Low",
                "Ofenbach_-_Be_Mine", "OMFG_-_Hello", "Shape_of_You", "Take_Me_to_Infinity", "Tell_It_To_My_Heart"};

        Media music = new Media(new File(musicsFile + nameSongs[new Random().nextInt(nameSongs.length)] + ".mp3").toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(music);
        mediaPlayer.play();

        return mediaPlayer;
    }
}
