package main.java.View.Tests;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import main.java.Core.model.Word;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class AppropTranslControl {

    @FXML
    AnchorPane pane;

    @FXML
    private Button buttonExitTest;

    private Stage currStage;

    List<Word> wordList = new ArrayList<>(); // list of words in current test (4 words)

    private VBox vBoxWords = new VBox();

    private VBox vBoxTransl = new VBox();

    private boolean oneButPressed = false;
    private Button buttonPressed = new Button();

    // for exit from test
    private String nameTheme;


    public void setNameTheme(String nameTheme) {
        this.nameTheme = nameTheme;
    }

    public void setCurrStage(Stage currStage) {
        this.currStage = currStage;
    }

    public void setWordList(List<Word> wordList) {
        this.wordList = wordList;
    }

    public void showTest() {
        createButWords();
        createButTranslWords();
    }


    @FXML
    private void hadleClickExitTestBut() {
        new TranslatingWordControl();
        if (!TranslatingWordControl.confirmExitFromTest()) // query on confirm exit from test
            return;
        TranslatingWordControl.openWordsView(currStage, nameTheme);
    }

    @FXML
    private void mouseMovedExTestBut() {
        buttonExitTest.setStyle("-fx-effect: inherit");
    }

    @FXML
    private void mouseExitExTestBut() {
        buttonExitTest.setStyle("");
    }

    private void createButWords() {
        List<Button> buttons = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Button button = new Button();
            button.setText(wordList.get(i).getWord());
            button.setFont(Font.font("System", 26));
            button.setPrefWidth(220);
            button.setPrefHeight(55);

            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    handlePressBut(button);
                }
            });
            buttons.add(button);
        }
        vBoxWords.setLayoutX(100);
        vBoxWords.setLayoutY(195);
        vBoxWords.setSpacing(30);
        // arr for rand fill buttons
        int[] arr = TranslatingWordControl.genRandArr(4);
        //
        vBoxWords.getChildren().add(buttons.get(arr[0]));
        vBoxWords.getChildren().add(buttons.get(arr[1]));
        vBoxWords.getChildren().add(buttons.get(arr[2]));
        vBoxWords.getChildren().add(buttons.get(arr[3]));

        // show hBox
        pane.getChildren().add(vBoxWords);
    }

    private void createButTranslWords() {
        List<Button> buttons = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Button button = new Button();
            button.setText(wordList.get(i).getTranslWord());
            button.setFont(Font.font("System", 26));
            button.setPrefWidth(220);
            button.setPrefHeight(55);

            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    handlePressBut(button);
                }


            });
            buttons.add(button);
        }


        vBoxTransl.setLayoutX(480);
        vBoxTransl.setLayoutY(195);
        vBoxTransl.setSpacing(30);
        // arr for rand fill buttons
        int[] arr = TranslatingWordControl.genRandArr(4);
        //
        vBoxTransl.getChildren().add(buttons.get(arr[0]));
        vBoxTransl.getChildren().add(buttons.get(arr[1]));
        vBoxTransl.getChildren().add(buttons.get(arr[2]));
        vBoxTransl.getChildren().add(buttons.get(arr[3]));

        // show hBox
        pane.getChildren().add(vBoxTransl);
    }

    // buttons logic handle
    private void handlePressBut(Button button) {
        // buttons logic
        if (!oneButPressed) {
            button.setStyle("-fx-effect: dropshadow(gaussian, lime, 10, 0.9, 0, 0)");
            buttonPressed = button;
            oneButPressed = true;
        } else { // check appropriate
            oneButPressed = false;
            if (getTranslWord(button.getText()).equals(buttonPressed.getText())) { // correctly answer

                // remove these buttons from scene
                vBoxWords.getChildren().remove(button);
                vBoxWords.getChildren().remove(buttonPressed);
                vBoxTransl.getChildren().remove(button);
                vBoxTransl.getChildren().remove(buttonPressed);
                // vbox is empty applause and exit
                if (vBoxWords.getChildren().isEmpty() & vBoxTransl.getChildren().isEmpty()) {
                    // congratulation
                    File curDir = new File(".");
                    String musicFile = curDir.getAbsolutePath() + "\\src\\resources\\correct.wav";
                    Media sound = new Media(new File(musicFile).toURI().toString());
                    MediaPlayer mediaPlayer = new MediaPlayer(sound);
                    mediaPlayer.play();
                    //
                    showNextTest();
                }


            } else { // incorrectly answer
                button.setStyle("-fx-effect: dropshadow(gaussian, red, 8, 0.9, 0, 0)");
                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        button.setStyle("");
                        buttonPressed.setStyle("");
                    }
                }, 200);
            }
        }

    }

    // get translated word for passed word from list word
    private String getTranslWord(String word) {
        for (Word wordObj : wordList) {
            if (wordObj.getWord().equals(word)) {
                return wordObj.getTranslWord();
            } else if (wordObj.getTranslWord().equals(word)) {
                return wordObj.getWord();
            }
        }
        return null; // such word doesn't exist
    }

    private void showNextTest() {
        RandTest.showRandTest(currStage, wordList, nameTheme);
    }
}

