package main.java.View.Authorization;


import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import main.java.Core.dao.ThemeDao;
import main.java.Core.dao.UserDao;
import main.java.Core.model.User;
import main.java.MainApp;

public class CreateNewAccountControl {
    @FXML
    private TextField userName;

    @FXML
    private TextField password;

    @FXML
    private Button buttonCrAcc;

    private Stage primaryStage;

    private Stage currStage;


    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void setCurrentStage(Stage currentStage) {
        this.currStage = currentStage;
    }


    @FXML
    private void initialize() {
        // bind <ENTER> with sign in

        userName.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
            if (ev.getCode() == KeyCode.ENTER) {
                handleCreateAccount();
            }
        });

        password.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
            if (ev.getCode() == KeyCode.ENTER) {
                handleCreateAccount();
            }
        });
    }

    @FXML
    private void mouseMoveButCrAcc() {
        buttonCrAcc.setStyle("-fx-effect: inherit");
    }

    @FXML
    private void mouseExitButCrAcc() {
        buttonCrAcc.setStyle("");
    }

    @FXML
    private void handleCreateAccount() {
        // check on correct input
        if (userName.getText().equals("") | password.getText().equals("")) {
            errorNoFillFields();
            return;
        }

        // check on existing account
        if (new UserDao().getUser(userName.getText()) != null) {
            userName.clear();
            errorExistSuchAccount();
            return;
        }

        // init user
        String temp = userName.getText();
        User user = new User();
        user.setName(userName.getText());
        user.setPassword(password.getText());
        user.setPoints(0);

        //  add user in data base
        UserDao userDao = new UserDao();
        userDao.addUser(user);

        // exit dialog stage
        currStage.close();

        // open choice of themes
        ThemeDao themeDao = new ThemeDao();
        StartPageControl control = new StartPageControl();
        control.showChoiceThemes(user, themeDao.listTheme(), primaryStage);

    }

    private void errorNoFillFields() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Ошибка при заполнении данных о пользователе");
        alert.setHeaderText("Имя пользователя или пароль не были введены");

        MainApp.addIconAlert(alert);

        alert.showAndWait();
    }

    private void errorExistSuchAccount() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Ошибка при пегистрации");
        alert.setHeaderText("Данная учетная запись уже существует");

        MainApp.addIconAlert(alert);

        alert.showAndWait();
    }

}

