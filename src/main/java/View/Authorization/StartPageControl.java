package main.java.View.Authorization;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import main.java.Core.dao.ThemeDao;
import main.java.Core.dao.UserDao;
import main.java.Core.model.Theme;
import main.java.Core.model.User;
import main.java.MainApp;
import main.java.View.ChoiceThemeAndWords.ChoiceThemesControl;

import java.io.IOException;
import java.util.List;


public class StartPageControl {
    private Stage currStage;

    @FXML
    private Label createAccount;

    @FXML
    private TextField userName;

    @FXML
    private TextField password;

    @FXML
    private Button buttonSignIn;


    public void showChoiceThemes(User user, List<Theme> themes, Stage currStage) {
        try {
            // load Choice Theme View form fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("View/ChoiceThemeAndWords/ChoiceThemesView.fxml"));
            AnchorPane anchorPane = (AnchorPane) loader.load();
            //

            // get controller and set user, themes
            ChoiceThemesControl controller = loader.getController();
            controller.setUser(user);
            controller.setThemes(themes);
            controller.showData();
            controller.setCurrStage(currStage);

            // set page on stage
            currStage.setScene(new Scene(anchorPane));
        } catch (IOException io) {
            MainApp.errorLoadFXML();
        }
    }

    public void setCurrStage(Stage currStage) {
        this.currStage = currStage;
    }


    @FXML
    private void initialize() {
        // bind <ENTER> with sign in

        userName.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
            if (ev.getCode() == KeyCode.ENTER) {
                handleSignIn();
            }
        });

        password.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
            if (ev.getCode() == KeyCode.ENTER) {
                handleSignIn();
            }
        });
    }

    @FXML
    private void handleSignIn() {
        UserDao userDao = new UserDao();

        // если запись существует - входим
        if (userDao.checkDataUser(userName.getText(), password.getText())) {
            ThemeDao themeDao = new ThemeDao(); // for get themes
            showChoiceThemes(userDao.getUser(userName.getText()), themeDao.listTheme(), currStage); // open choice of themes
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка при входе в учетную запись");
            alert.setHeaderText("Не удается войти.\n" +
                    "Пожалуйста, проверьте правильность написания логина и пароля.");

            MainApp.addIconAlert(alert);

            alert.showAndWait();
        }
    }

    @FXML
    private void handleCreateAccount() {
        try {
            // load Choice Theme View form fxml file
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(StartPageControl.class.getResource("CreateNewAccountView.fxml"));
            AnchorPane anchorPane = (AnchorPane) loader.load();
            //

            // Create dialog window Stage for create account
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Новый аккаунт");
            dialogStage.initOwner(currStage);
            dialogStage.getIcons().add(new Image("/resources/Images/IconApp.png"));
            Scene scene = new Scene(anchorPane);
            dialogStage.setScene(scene);
            // access to Main App
            CreateNewAccountControl controller = loader.getController();
            ;
            controller.setCurrentStage(dialogStage);
            controller.setPrimaryStage(currStage);

            // Show dialog stage and wait input user
            dialogStage.showAndWait();


        } catch (IOException io) {
            MainApp.errorLoadFXML();
        }
    }

    @FXML
    private void mouseMoveCrAccount() {
        createAccount.setStyle("-fx-border-width: 1px;\n" +
                "    -fx-border-color: #3d35ff;\n" +
                "    -fx-background-color: derive(silver, 20%);");
    }

    @FXML
    private void mouseExitedCrAccount() {
        createAccount.setStyle("");
    }

    @FXML
    private void mouseMoveSignIn() {
        buttonSignIn.setStyle("-fx-effect: inherit");
    }

    @FXML
    private void mouseExitedSignIn() {
        buttonSignIn.setStyle("");
    }
}
