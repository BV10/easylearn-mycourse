package main.java.View.Grammar;


import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.stage.Stage;
import main.java.Core.dao.ThemeDao;
import main.java.View.Authorization.CreateNewAccountControl;
import main.java.View.Authorization.StartPageControl;
import main.java.View.ChoiceThemeAndWords.ChoiceThemesControl;

public class CreateEngSentControl {

    @FXML
    private Button buttonOK;

    private Stage currStage;


    public void setCurrStage(Stage currStage) {
        this.currStage = currStage;
    }


    @FXML
    private void handleButOK() {
        // show themes view
        new StartPageControl().showChoiceThemes(ChoiceThemesControl.getUser(), new ThemeDao().listTheme(), currStage);
    }

    @FXML
    private void mouseMovedButOK() {
        buttonOK.setStyle("-fx-effect: inherit");
    }

    @FXML
    private void mouseExitButOK() {
        buttonOK.setStyle("");
    }

}
