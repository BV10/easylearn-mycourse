package main.java.View.ChoiceThemeAndWords;


import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.java.Core.model.Theme;
import main.java.Core.model.User;
import main.java.MainApp;
import main.java.View.Authorization.StartPageControl;
import main.java.View.Grammar.CreateEngSentControl;
import java.io.IOException;
import java.util.*;

public class ChoiceThemesControl {
    private static User user = new User();
    private List<Theme> themes = new ArrayList<>();
    private Stage currStage;

    @FXML
    private AnchorPane pane;

    @FXML
    private Label labelUser;
    @FXML
    private Label labelPoints;


    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        ChoiceThemesControl.user = user;
    }

    public void setThemes(List<Theme> themes) {
        this.themes = themes;
    }

    public void setCurrStage(Stage currStage) {
        this.currStage = currStage;
    }

    public void showData() {
        labelUser.setText(user.getName());
        labelPoints.setText(Long.toString(user.getPoints()));

        // highlight buttons during move mouse
        VBox vBox = (VBox) pane.getChildren().get(3);
        ObservableList<Node> buttons = vBox.getChildren();
        for (Node node : buttons) {
            Button button = (Button) node;
            button.setDisable(true); // disable all buttons
            button.setOnMouseMoved(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    button.setStyle("-fx-effect: inherit");
                }
            });

            button.setOnMouseExited(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    button.setStyle("");
                }
            });
        }
        //

        for (int i = 0; i < user.getLearntThemes() + 1; i++) {
            buttons.get(i).setDisable(false);
        }

        Button butExitAcc = (Button) pane.getChildren().get(5);
        butExitAcc.setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                butExitAcc.setStyle("-fx-effect: inherit");
            }
        });

        butExitAcc.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                butExitAcc.setStyle("");
            }
        });
    }


    @FXML
    private void handleBaseWords() {
        showWords(themes.get(1));
    }

    @FXML
    private void handleCreateSent() {
        // open create sent FXML
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("View/Grammar/CreateEngSentView.fxml"));
            ScrollPane pane = (ScrollPane) loader.load();

            //new Timer().schedule(new TimerTask() {
            //    @Override
            //    public void run() {
            pane.setVvalue(0.0);
            //     }
            // }, 200);

            CreateEngSentControl control = loader.getController();
            control.setCurrStage(currStage);

            currStage.setScene(new Scene(pane));
        } catch (IOException io) {
            MainApp.errorLoadFXML();
        }
    }

    @FXML
    private void handleSubst() { // существительные
        showWords(themes.get(2));
    }

    @FXML
    private void handleAdject() {// прилагательные
        showWords(themes.get(3));
    }

    // глаголы
    @FXML
    private void handleVerb() {
        // open create sent FXML
        showWords(themes.get(4));
    }

    @FXML
    private void handleExitFromAcc() {
        if (!confirmExitFromAcc()) // get confirmation on exit from user
            return;

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("View/Authorization/StartPageView.fxml"));
            AnchorPane panePrimary = loader.load();

            StartPageControl control = loader.getController();
            control.setCurrStage(currStage);

            currStage.setScene(new Scene(panePrimary));
        } catch (IOException io) {
            MainApp.errorLoadFXML();
        }

    }

    private boolean confirmExitFromAcc() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        MainApp.addIconAlert(alert);
        alert.setTitle("Подтверждение");
        alert.setHeaderText("Вы хотите покинуть данный аккаунт?");
        Optional<ButtonType> result = alert.showAndWait(); // get pressed button
        if (result.get() == ButtonType.OK) {
            return true;
        } else {
            return false;
        }
    }

    private void showWords(Theme theme) {
        // open view words FXML
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ChoiceThemesControl.class.getResource("ChoiceWordView.fxml"));
            AnchorPane pane = (AnchorPane) loader.load();

            ChoiceWordControl control = loader.getController();
            control.setCurrStage(currStage);
            control.setCurrTheme(theme);
            control.showWords();

            currStage.setScene(new Scene(pane));
            currStage.show();
        } catch (IOException io) {
            MainApp.errorLoadFXML();
        }
    }
}
