package main.java.View.ChoiceThemeAndWords;


import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import main.java.Core.dao.ThemeDao;
import main.java.Core.model.Theme;
import main.java.Core.model.Word;
import main.java.MainApp;
import main.java.View.Tests.IntroductionWithWordControll;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ChoiceWordControl {
    private Stage currStage;
    private Theme currTheme; // current theme
    List<Button> buttonList = new ArrayList<>(10);

    @FXML
    AnchorPane pane;

    @FXML
    ImageView imageViewBack;


    public void setCurrTheme(Theme currTheme) {
        this.currTheme = currTheme;
    }

    public void setCurrStage(Stage currStage) {
        this.currStage = currStage;
    }

    @FXML
    public void showWords() {
        // create buttons with words
        Iterator<Word> wordIterator = currTheme.getWordList().iterator();

        for (int i = 0; i < 10; i++) {
            Button button = new Button();

            button.setDisable(true);// disable all buttons
            // set up button
            button.addEventHandler(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    button.setStyle("-fx-effect: inherit");
                }
            });
            button.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    button.setStyle("");
                }
            });
            button.setFont(Font.font("Elephant", 21));
            button.setTextAlignment(TextAlignment.CENTER);
            button.setPrefWidth(130);
            button.setPrefHeight(160);
            button.setWrapText(true); // wrap lines

            // get 4 words and save in str
            int j = 0;
            String words = new String();

            while (wordIterator.hasNext() && j < 4) {
                words += wordIterator.next().getWord() + "\n";
                j++;

            }
            //

            //save in button text and add to listbutton
            button.setText(words);
            //add handler
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    // add words in list
                    List<Word> wordList = new ArrayList<>();
                    String[] strWords = button.getText().split("\n");
                    for (int i = 0; i < strWords.length; i++) {
                        for (Word word : currTheme.getWordList()) {
                            if (strWords[i].equals(word.getWord()) && wordList.size() < 4) {
                                wordList.add(word);
                                break;
                            }
                        }
                    }
                    //


                    showFirstTest(wordList);
                }
            });

            buttonList.add(button);
        }

        //do able learnt block buttons
        for (int i = 0; i < ChoiceThemesControl.getUser().getLearnBlockWords().get(currTheme.getName()); i++) {
            buttonList.get(i).setDisable(false);
        }

        addButtonsInHBox();
    }


    @FXML
    private void handleBack() {
        try {
            // open ThemesView
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("View/ChoiceThemeAndWords/ChoiceThemesView.fxml"));
            AnchorPane paneTheme = loader.load();

            ChoiceThemesControl control = loader.getController();
            control.setThemes(new ThemeDao().listTheme());
            control.showData();
            control.setCurrStage(currStage);

            currStage.setScene(new Scene(paneTheme));
        } catch (IOException io) {
            MainApp.errorLoadFXML();
        }
        // get themes from database

    }

    @FXML
    private void mouseMovedImageBack() {
        File currDir = new File("");
        imageViewBack.setImage(new Image(currDir.toURI().toString() + "\\src\\resources\\Images\\Back2.png"));
    }

    @FXML
    private void mouseExitImageBack() {
        File currDir = new File("");
        imageViewBack.setImage(new Image(currDir.toURI().toString() + "\\src\\resources\\Images\\Back.png"));
    }

    private void addButtonsInHBox() {
        // add buttons to Hboxes
        HBox hBox1 = new HBox();
        hBox1.setSpacing(30);
        hBox1.setLayoutX(120);
        hBox1.setLayoutY(225);
        for (int i = 0; i < 5; i++) {
            hBox1.getChildren().add(buttonList.get(i));
        }

        HBox hBox2 = new HBox();
        hBox2.setSpacing(30);
        hBox2.setLayoutX(120);
        hBox2.setLayoutY(394);
        for (int i = 5; i < 10; i++) {
            hBox2.getChildren().add(buttonList.get(i));
        }

        // show Hboxes
        pane.getChildren().add(hBox1);
        pane.getChildren().add(hBox2);
    }

    private void showFirstTest(List<Word> wordList) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("View/Tests/IntroductionWithWord.fxml"));
            AnchorPane pane = (AnchorPane) loader.load();

            IntroductionWithWordControll controll = loader.getController();
            controll.setTheme(currTheme);
            controll.setWordList(wordList);
            controll.setCurrStage(currStage);
            controll.showIntroduct();

            currStage.setScene(new Scene(pane));

        } catch (IOException io) {

            MainApp.errorLoadFXML();
        }
    }


}
