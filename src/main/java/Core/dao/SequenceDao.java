package main.java.Core.dao;

import com.mongodb.MongoClient;
import main.java.Core.model.Sequence;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

// class for create unique ID in data base
public class SequenceDao {

    // create unique id
    public static int getNextSequenceId(String key) {

        Query query = new Query(new Criteria("id").is(key));

        // enlarge sequence on 1
        Update update = new Update();
        update.inc("sequence", 1);

        // create option for return updated object
        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true);

        Sequence sequence = getMongoTemplate().findAndModify(query, update, options, Sequence.class, key);

        return sequence.getSequence();
    }

    // get mongo template
    private static MongoTemplate getMongoTemplate() {
        return new MongoTemplate(new SimpleMongoDbFactory(new MongoClient(), "english"));
    }

}
