package main.java.Core.dao;

import main.java.Core.model.User;
import com.mongodb.MongoClient;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;


public class UserDao {

    public static String COLLECTION_NAME = "users";

    public void addUser(User user) {
        if (getUser(user.getName()) == null) {
            user.setId(String.valueOf(SequenceDao.getNextSequenceId(COLLECTION_NAME)));
            getMongoTemplate().save(user, "users");
        }
    }

    public User getUser(String name) {
        Query query = new Query(new Criteria("name").is(name));
        return getMongoTemplate().findOne(query, User.class, COLLECTION_NAME);
    }

    public boolean checkDataUser(String userName, String password) {
        Query query = new Query(new Criteria("name").is(userName));
        User user = getMongoTemplate().findOne(query, User.class, COLLECTION_NAME);

        // проверяем имя и пароль
        if (user != null && user.getPassword().equals(password)) {
            return true;
        } else {
            return false;
        }
    }

    private MongoTemplate getMongoTemplate() {
        return new MongoTemplate(new SimpleMongoDbFactory(new MongoClient(), "english"));
    }

    public Long updateUserPoints(User user) {

        //update user in database

        Query query = new Query(new Criteria("id").is(user.getId()));

        // update points
        Update update = new Update();
        update.inc("points", 20);
        //

        FindAndModifyOptions modifyOptions = new FindAndModifyOptions();
        modifyOptions.returnNew(true);

        User updateUser = getMongoTemplate().findAndModify(query, update, modifyOptions, User.class, "users");

        return updateUser.getPoints();
    }

    public void updateUser(User user) {

        getMongoTemplate().save(user, "users");
    }

}
