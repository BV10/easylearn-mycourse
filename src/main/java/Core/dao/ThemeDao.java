package main.java.Core.dao;

import com.mongodb.MongoClient;
import main.java.Core.model.Theme;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import java.util.List;

public class ThemeDao {

    public static String COLLECTION_NAME = "theme";

    public List<Theme> listTheme() {
        return getMongoTemplate().findAll(Theme.class, "theme");
    }

    public void addTheme(Theme theme) {
        if (getTheme(theme.getName()) == null) {
            theme.setId(String.valueOf(SequenceDao.getNextSequenceId(COLLECTION_NAME)));
            getMongoTemplate().insert(theme, COLLECTION_NAME);
        }
    }

    public Theme getTheme(String name) {
        Query query = new Query(new Criteria("name").is(name));
        return getMongoTemplate().findOne(query, Theme.class, COLLECTION_NAME);
    }

    private MongoTemplate getMongoTemplate() {
        return new MongoTemplate(new SimpleMongoDbFactory(new MongoClient("127.0.0.1"), "english"));
    }

}
