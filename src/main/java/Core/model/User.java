package main.java.Core.model;

import org.springframework.data.annotation.Id;
import java.util.*;

public class User {

    @Id
    private String id = "";

    private String name = new String();

    private String password = new String();

    private long points = 0; // quantity points

    private int accessedThemes = 1; // quantity learnt themes

    private Map<String, Integer> learnBlockWords = new HashMap<>();

    public User(){
        learnBlockWords.put("BaseWords", 1);
    }

    public void setLearntThemes(int learntThemes) {
        this.accessedThemes = learntThemes;
    }

    public int getLearntThemes() {
        return accessedThemes;
    }

    public Map<String, Integer> getLearnBlockWords() {
        return learnBlockWords;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

}
