package main.java.Core.model;

public class Word {

    // слово
    private String word = new String();

    // перевод слова
    private String translWord = new String();

    // предложение
    private String sentence = new String();


    public Word(String word, String translWord, String sentence) {
        this.word = word;
        this.translWord = translWord;
        this.sentence = sentence;
    }

        public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getTranslWord() {
        return translWord;
    }

    public String getSentence() {
        return sentence;
    }

}
