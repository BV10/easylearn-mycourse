package main.java.Core.model;

        import org.springframework.data.annotation.Id;
        import java.util.ArrayList;

public class Theme {

    @Id
    private String id;

    String name;

    // список слов
    private ArrayList<Word> wordList = new ArrayList<>();


    public Theme(String name) {
        this.name = name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Word> getWordList() {
        return wordList;
    }

    public void setWordList(ArrayList<Word> wordList) {
        this.wordList = wordList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Theme theme = (Theme) o;

        return id.equals(theme.id);
    }
}
